function createNewUser() {
    let newUser = {
        firstName: prompt('Enter Your  first name: ', ''),
        lastName: prompt('Enter Your last name:', ''),
        birthday: (prompt('Enter Your Birth Year  DD.MM.YYYY:')),

        getLogin: function () {
            return newUser.firstName.charAt(0).toLowerCase() + newUser.lastName.toLowerCase();

        },
        getAge: function () {
            let currentYear = new Date();
            return Math.abs(currentYear.getFullYear() - parseInt(this.birthday.slice(-4)))
        },

        getPassword: function () {
            return newUser.firstName.charAt(0).toUpperCase() + newUser.lastName.toLowerCase() + (this.birthday.slice(-4));
        }
    }
    return newUser;
}

let newUser = createNewUser();

console.log(`${newUser.getLogin()}`);
console.log(newUser.getAge());

console.log(newUser.getPassword());


